<?php

namespace App\Http\Controllers;

use App\Models\CvStaff;
use App\Models\Staff_Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL; 
use Illuminate\Support\Facades\DB;

class CvStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $basenameUrl = basename(URL::current());

        //$cvList = CvStaff::all(); 

        //$cvList = CvStaff::orderBy('id', 'DESC')->get();

        $category_id = $request->input('category_id');


        $staff_categories = Staff_Category::all();


         

        if (!empty($category_id)) {

         $cvList = DB::table('cv_staff')
                ->select(
                    'cv_staff.*',
                    'staff_categories.category_name'
                )
                ->join('staff_categories', 'staff_categories.id', '=', 'cv_staff.category_id')
                ->Where('category_id',  $category_id)
                ->get();
    }else{
         $cvList = DB::table('cv_staff')
                ->select(
                    'cv_staff.*',
                    'staff_categories.category_name'
                )
                ->join('staff_categories', 'staff_categories.id', '=', 'cv_staff.category_id')              
                ->get();

       // $cvList = DB::select('SELECT * FROM cv_staff');
    }

        //  echo '<pre>';
        // print_r($cvList);
        // exit();

        return view('pages/staff_cv/indexCv', compact('basenameUrl','cvList','staff_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $basenameUrl = basename(URL::current());

        $staff_categories = Staff_Category::all();

        //$staff_categories = Staff_Category::where('id', 'id')->get();

        return view('pages/staff_cv/createStaffCv', compact('basenameUrl', 'staff_categories'));
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    
        echo '<pre>';
        print_r($diffYears);
        exit();

        $data = array();
        $data['category_id'] = $request->category_id;
        $data['consultant_name'] = $request->consultant_name;
        $data['rfp_no'] = $request->rfp_no;
        $data['client_name'] = $request->client_name;
        $data['position'] = $request->position;
        $data['staff_name'] = $request->staff_name;
        $data['birth_date'] = $request->birth_date;
        $data['nationality'] = $request->nationality;
        $data['membership'] = $request->membership;
        $data['qualification'] = $request->qualification;
        $data['experience'] = $request->experience;
        $data['training'] = $request->training;
        $data['country_w_experience'] = $request->country_w_experience;
        $data['employer'] = $request->employer;
        $data['duration'] = $request->duration;
        $data['employment_position'] = $request->employment_position;
        $data['duties'] = $request->duties;
      

            $image = $request->file('cv_file');

             $imageURL='';

        if ($image){ 
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $imageFullName = $image_name.'.'.$ext;
            $uploadPath = 'media/';
            $imageURL = $uploadPath.$imageFullName;
            $success = $image->move($uploadPath,$imageFullName);    



    }
     // $data['n_file'] = (!empty($imageURL))?$imageURL:'';

// 
        $data['cv_file'] = $imageURL;

        $Stdata = DB::table('cv_staff')->insert($data);

        return redirect()->route('staffcv')->with('success', 'Successfully Add');


        // $requestData = $request->all();

        // if (CvStaff::create($requestData)) {
        //     return redirect()->route('staffcv')->with('success', 'Successfully Add');
        // }
        // return back()->with('failed', 'Something went wrong. Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CvStaff  $cvStaff
     * @return \Illuminate\Http\Response
     */
    public function show(CvStaff $cvStaff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CvStaff  $cvStaff
     * @return \Illuminate\Http\Response
     */
    public function edit(CvStaff $cvStaff, $id)
    {
        $basenameUrl = basename(URL::current());
        $staff_categories = Staff_Category::all();
        $cvData = DB::table('cv_staff')->where('id', $id)->first();

        return view('pages/staff_cv/editStaffCv', compact('basenameUrl', 'staff_categories','cvData'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CvStaff  $cvStaff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CvStaff $cvStaff, $id)
    {

        $data = array();
        $data['category_id'] = $request->category_id;
        $data['consultant_name'] = $request->consultant_name;
        $data['rfp_no'] = $request->rfp_no;
        $data['client_name'] = $request->client_name;
        $data['position'] = $request->position;
        $data['staff_name'] = $request->staff_name;
        $data['birth_date'] = $request->birth_date;
        $data['nationality'] = $request->nationality;
        $data['membership'] = $request->membership;
        $data['qualification'] = $request->qualification;
        $data['experience'] = $request->experience;
        $data['training'] = $request->training;
        $data['country_w_experience'] = $request->country_w_experience;
        $data['employer'] = $request->employer;
        $data['duration'] = $request->duration;
        $data['employment_position'] = $request->employment_position;
        $data['duties'] = $request->duties;


         $image = $request->file('cv_file');
 
        if ($image != null){ 
            // unlink($PreviousFile);
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $imageFullName = $image_name.'.'.$ext;
            $uploadPath = 'media/';
            $imageURL = $uploadPath.$imageFullName;
            $success = $image->move($uploadPath,$imageFullName);

        $data['cv_file'] = $imageURL;
        $cvdata = DB::table('cv_staff')->where('id', $id)->update($data);

        return redirect()->route('staffcv')
                         ->with('success','Updated! The Data Updated Successfully');

        }else{
            $cvdata = DB::table('cv_staff')->where('id', $id)->update($data);

        return redirect()->route('staffcv')
                         ->with('success','Updated! The Data Updated Successfully');
        }


        // $requestData = $request->all();

        //  $cvStaff->update($requestData);

        //   return redirect()->route('staffcv')
        //     ->with('success', 'CV updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CvStaff  $cvStaff
     * @return \Illuminate\Http\Response
     */
    public function destroy(CvStaff $cvStaff, $id)
    {
        $cvdata = DB::table('cv_staff')->where('id', $id)->delete();

        return redirect()->route('staffcv')
            ->with('success', 'CV deleted successfully');
    }
}
