<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CvStaff;
use App\Models\Project;
use Illuminate\Support\Facades\URL; 
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Carbon;


class ProjectController extends Controller
{
    public function index(Request $request)
    {

        $basenameUrl = basename(URL::current());

        // $projectList = Project::orderBy('id', 'DESC')->get();

        $start_date = $request->input('start_date');

        $end_date = $request->input('end_date');

        if (!empty($start_date) && ($end_date)) {

        $datewiseProjectList = DB::table('projects')
        		->select('projects.*')
        		->orderBy('id', 'DESC')
        		->where('start_date',   $start_date)
                ->where('end_date',  $end_date)
                ->get();
            }
           else
           {
           	 $datewiseProjectList = DB::table('projects')
        		->select('projects.*')
        		->orderBy('id', 'DESC')
                ->get();
           }
      
        return view('pages/projects/indexProject', compact('basenameUrl','datewiseProjectList'));
    }


     public function create(){

     	$basenameUrl = basename(URL::current());

        $staff = CvStaff::all();

    	return view('pages/projects/createProject', compact('basenameUrl','staff'));
    }

    public function yearMonth($date_convert){

$dateMonthArray = explode('/', $date_convert);

$month = $dateMonthArray[0];
$year = $dateMonthArray[1];


return $date = Carbon::createFromDate($year, $month,);

 
    }

     public function insert(Request $request){  
   

$start_date = $request->input('start_date'); 
$end_date = $request->input('end_date'); 


    	$data = array();
    	$data['project_name'] = $request->project_name;
        $data['country'] = $request->country;
        $data['in_location'] = $request->in_location;
        $data['client_name'] = $request->client_name;
        $data['staff_months'] = $request->staff_months;
        $data['jv_consultant'] = $request->jv_consultant;
        $data['jv_staff_months'] = $request->jv_staff_months;       
    	$data['project_description'] = $request->project_description;
    	$data['project_cost'] = $request->project_cost;    	
    	$data['start_date'] =  $this->yearMonth($start_date);       
    	$data['end_date'] = $this->yearMonth($end_date);
    	$data['service_render'] = $request->service_render;
    	$data['remarks'] = $request->remarks;
        $data['firm_name'] = $request->firm_name;
      //  $data['assign_staffs'] = implode(',', $request->assign_staffs);

       

        

    		$image = $request->file('n_file');

             $imageURL='';

    	if ($image){ 
    	    $image_name = date('dmy_H_s_i');
    	    $ext = strtolower($image->getClientOriginalExtension());
    	    $imageFullName = $image_name.'.'.$ext;
    	    $uploadPath = 'media/';
    	    $imageURL = $uploadPath.$imageFullName;
    	    $success = $image->move($uploadPath,$imageFullName);    



    }
     // $data['n_file'] = (!empty($imageURL))?$imageURL:'';

// 
        $data['n_file'] = $imageURL;

        $Stdata = DB::table('projects')->insert($data);


        return redirect()->route('project');
    					 
    }

        public function edit($id)
    {

    	$basenameUrl = basename(URL::current());
    	$projectData = DB::table('projects')->where('id', $id)->first();

   //      echo '<pre>';
   //      print_r($projectData->n_file);
   //      exit();

        return view('pages/projects/editProject', compact('projectData','basenameUrl'));
    }

    public function show($id){

    	$basenameUrl = basename(URL::current());
    	$pdata = DB::table('projects')->where('id', $id)->first();
    	return view('pages/projects/showProject2', compact('pdata','basenameUrl'));
    }

     public function update(Request $request, $id){

        $start_date_test =$request->input('start_date'); 
        $end_date_test =$request->input('end_date'); 

        $PreviousFile = $request->Prev_file;

    	$data = array();
    	$data['project_name'] = $request->project_name;
        $data['country'] = $request->country;
        $data['in_location'] = $request->in_location;
        $data['client_name'] = $request->client_name;
        $data['staff_months'] = $request->staff_months;
        $data['jv_consultant'] = $request->jv_consultant;
        $data['jv_staff_months'] = $request->jv_staff_months; 
    	$data['project_description'] = $request->project_description;
    	$data['project_cost'] = $request->project_cost;
    	$data['start_date'] =  $this->yearMonth($start_date_test);      
        $data['end_date'] = $this->yearMonth($end_date_test);
    	$data['service_render'] = $request->service_render;
    	$data['remarks'] = $request->remarks;
        $data['firm_name'] = $request->firm_name;
        $data['assign_staffs'] = $request->assign_staffs;


        $image = $request->file('n_file');
 
        if ($image != null){ 
            // unlink($PreviousFile);
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $imageFullName = $image_name.'.'.$ext;
            $uploadPath = 'media/';
            $imageURL = $uploadPath.$imageFullName;
            $success = $image->move($uploadPath,$imageFullName);

        $data['n_file'] = $imageURL;
        $prdata = DB::table('projects')->where('id', $id)->update($data);

        return redirect()->route('project')
                         ->with('success','Updated! The Data Updated Successfully');

        }else{
            $prdata = DB::table('projects')->where('id', $id)->update($data);

        return redirect()->route('project')
                         ->with('success','Updated! The Data Updated Successfully');
        }

    	}

    	public function delete($id){

    		$data = DB::table('projects')->where('id', $id)->first();
    		$prdata = DB::table('projects')->where('id', $id)->delete();
    		return redirect()->route('project')
    						 ->with('success','Deleted! The Data Deleted Successfully');
    	}

}
