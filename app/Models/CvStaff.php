<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CvStaff extends Model
{
    use HasFactory;

    protected $guarded = [];

     public function staff_category()
    {
    	return $this->belongTo('App\Staff_Category', 'category_id', 'id');
    }
}
