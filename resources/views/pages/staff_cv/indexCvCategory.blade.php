@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>

    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'print',
            'pdf'
        ]
    } );
} );

</script>


@endsection


@section('css')

 <style>
        .portlet.box .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
        }

        .dataTables_wrapper .dt-buttons {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            /* margin: 0; */
            white-space: nowrap;
            /* text-align: right; */
            float: right !important;
        }

        .input-group-sm > .input-group-btn > select.btn, .input-group-sm > select.form-control, .input-group-sm > select.input-group-addon, select.input-sm {
            height: 31px;
            line-height: 30px;
        }

        .modal-dialog {
  padding-top: 10% !important;
}
    </style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">
        @can('create-category') 
         <div class="col-md-8" style="margin-top: 30px;">
        @else
               <div class="col-md-12" style="margin-top: 30px;">
@endcan
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                                <i class="fa fa-list"></i>Category List
                            </div>

                    <div class="dt-buttons">
                        
                    </div>

                </div>


                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th> Sl No.</th>
                                <th> Name and Location</th>
                                
                                <th> Action</th>
                            </tr>
                        </thead>
                        <tbody>


                           @foreach($categoryList as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->category_name}}</td>

                                <td style="width: 16%; text-align:center">  
                                    <a class="btn btn-primary" href="{{route('editStaffcvCategory',$item->id)}}"><i class="fa fa-edit"></i></a>
                                                               
                                    <!-- <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-edit"></i></button> -->
                                    @can('delete-category')
                                    <a class="btn btn-danger" href="{{route('deleteStaffcvCategory',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                    @endcan

                                </td>

                            </tr>
                            @endforeach
                     


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

@can('create-category') 
          <div class="col-md-4">
        <div class="tabbable-line boxless tabbable-reversed">

            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">

                    {{-- <div class="portlet box blue-hoki">--}}
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-file-text"></i>Category Form
                            </div>
                          

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{route('storeCategory')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name</label>
                                        <div class="col-md-8">
                                            <input name="category_name" value="" type="text" class="form-control" placeholder="Enter Category Name">


                                            {!! $errors->first('brand_name', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>                             

                                </div>

                                <div class="form-actions top">
                                    <div class="row">
                                        <div class="col-md-offset-4 col-md-8">
                                            <button type="submit" class="btn green">Submit</button>
                                        
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endcan
        

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
                <div class="tabbable-line boxless tabbable-reversed">

            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">

                    {{-- <div class="portlet box blue-hoki">--}}
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-file-text"></i>Category Edit Form
                            </div>
                          

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name</label> 
                                        <div class="col-md-8">
                                            <input name="category_name" value="" type="text" class="form-control" placeholder="Enter Category Name">


                                            {!! $errors->first('brand_name', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>                             

                                </div>

                                <div class="form-actions top">
                                    <div class="row">
                                        <div class="col-md-offset-4 col-md-8">
                                            <button type="submit" class="btn green">Submit</button>
                                        
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        </div>
        <div class="modal-footer">
          <button style="background-color: black; color: white;" type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>



</div>
</div>

@endsection
