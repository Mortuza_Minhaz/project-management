@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>

</script>


@endsection


@section('css')

<style>

	        .modal-dialog {
  padding-top: 10% !important;
}
    </style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

    	           <div class="tabbable-line boxless tabbable-reversed">

            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">

                    {{-- <div class="portlet box blue-hoki">--}}
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-file-text"></i>Category Edit Form
                            </div>
                          

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{route('updateStaffcvCategory',$cvCategorydata->id)}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name</label> 
                                        <div class="col-md-8">
                                            <input name="category_name" value="{{$cvCategorydata->category_name}}" type="text" class="form-control" placeholder="Enter Category Name">


                                            {!! $errors->first('brand_name', '<small class="text-danger">:message</small>') !!}
                                        </div>
                                    </div>                             

                                </div>

                                <div class="form-actions top">
                                    <div class="row">
                                        <div class="col-md-offset-4 col-md-8">
                                            <button type="submit" class="btn green">Submit</button>
                                        
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>




@endsection