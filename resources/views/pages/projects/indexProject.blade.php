@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>
     $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'print',
            'pdf'
        ]
    } );
} );
</script>


@endsection


@section('css')

<style>
     .portlet.box .dataTables_wrapper .dt-buttons {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        .dataTables_wrapper .dt-buttons {
            float: left;
        }

        div.dataTables_wrapper div.dataTables_paginate {
            /* margin: 0; */
            white-space: nowrap;
            /* text-align: right; */
            float: right !important;
        }

        .input-group-sm > .input-group-btn > select.btn, .input-group-sm > select.form-control, .input-group-sm > select.input-group-addon, select.input-sm {
            height: 31px;
            line-height: 30px;
        }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">



        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">

                    <div class="dt-buttons" style="margin-top: 5px;">
                        @can('create-project')
                        <a class="dt-button buttons-print btn default" tabindex="0" aria-controls="sample_2" href="{{route('addProject')}}"><span> <i class="fa fa-plus"></i>&nbsp; Add Project</span>

                        </a>
                        @endcan
                    </div>

                </div>

                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th style="text-align: center;"> Sl No.</th>
                                <th style="text-align: center;"> Project Name</th>
                                <th style="text-align: center;"> Location of the Project</th>
                                <th style="text-align: center;"> Project Description</th>
                                <th style="text-align: center;"> Project Cost</th>
                                <th style="text-align: center;"> Start Date</th>
                                <th style="text-align: center;"> Completion Date</th>
                                <th style="text-align: center;"> Service Render</th>
                                <th > Remarks</th>
                                <th style="text-align: center;"> Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach($datewiseProjectList as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{!! Str::limit($item->project_name, 100, ' .....') !!}</td>
                                <td>{!! Str::limit($item->in_location, 100, ' .....') !!}, {!! Str::limit($item->country, 100, ' .....') !!}</td>

                                <td style="width: 20%;">{!! Str::limit($item->project_description, 120, ' .....') !!}</td>
                                <td>{{$item->project_cost}}</td>

                                <td>{{ date('F-Y', strtotime($item->start_date)) }}</td>
                                
                                <td>{{ date('F-Y', strtotime($item->end_date)) }}</td>


                                <td>{!! Str::limit($item->service_render, 100, ' .....') !!}</td>
                                <td>{{$item->remarks}}</td>


                                <td style="width: 16%; text-align:center">
                                     <!--  <a href="{{$item->n_file}}" download="{{$item->n_file}}"> <button type="button" class="btn btn-primary"><i class="fa fa-cloud-download" ></i> </button></a> -->
                                    <a class="btn btn-primary" href="{{route('showProject',$item->id)}}"><i class="fa fa-info-circle"></i> </a>
                                    @can('edit-project')
                                    <a class="btn btn-primary" href="{{route('editProject',$item->id)}}"><i class="fa fa-edit"></i></a>
                                    @endcan

                                    @can('delete-project')
                                    <a class="btn btn-danger" href="{{route('deleteProject',$item->id)}}" onclick="return confirm('Are You Sure?')"><i class="fa fa-trash"></i></a>
                                    @endcan

                                </td>

                            </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</div>
<!-- END CONTENT BODY -->
@endsection