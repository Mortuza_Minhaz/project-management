@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-upload-wrap').hide();

                $('.file-upload-image').attr('src', e.target.result);
                $('.file-upload-content').show();

                $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function() {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function() {
        $('.image-upload-wrap').removeClass('image-dropping');
    });
</script>


@endsection

@section('css')

<style>
    div,
    form,
    input,
    select,
    textarea,
    label {
        padding: 0;
        margin: 0;
        outline: none;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        color: #666;
        line-height: 22px;
    }

    textarea {
        width: calc(100% - 12px);
        padding: 5px;
    }

    .testbox {
        display: flex;
        justify-content: center;
        align-items: center;
        height: inherit;
        padding: 20px;
    }

    form {
        width: 100%;
        padding: 20px;
        border-radius: 6px;
        background: #fff;
        box-shadow: 0 0 8px #006622;
    }

    input,
    select,
    textarea {
        margin-bottom: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    input {
        width: calc(100% - 10px);
        padding: 5px;
    }

    input[type="date"] {
        padding: 4px 5px;
    }

    textarea {
        width: calc(100% - 12px);
        padding: 5px;
    }

    .item:hover p,
    .item:hover i,
    .question:hover p,
    .question label:hover,
    input:hover::placeholder {
        color: #006622;
    }

    .item input:hover,
    .item select:hover,
    .item textarea:hover {
        border: 1px solid transparent;
        box-shadow: 0 0 3px 0 #006622;
        color: #006622;
    }

    .item {
        position: relative;
        margin: 10px 0;
    }

    .item span {
        color: red;
    }

    .item i,
    input[type="date"]::-webkit-calendar-picker-indicator {
        position: absolute;
        font-size: 20px;
        color: #00b33c;
    }

    .item i {
        right: 1%;
        top: 30px;
        z-index: 1;
    }

    .columns {
        display: flex;
        justify-content: space-around;
        flex-direction: row;
        flex-wrap: wrap;
    }

    .columns div {
        width: 48%;
    }

    [type="date"]::-webkit-calendar-picker-indicator {
        right: 1%;
        z-index: 8;
        opacity: 0;
        cursor: pointer;
    }

    .btn-block {
        margin-top: 10px;
        text-align: center;
        color: white;
    }

    button {
        width: 150px;
        padding: 10px;
        border: none;
        border-radius: 5px;
        background: #006622;
        font-size: 20px;

        cursor: pointer;
    }

    button:hover {
        background: #00b33c;
    }

    @media (min-width: 568px) {

        .name-item,
        .city-item {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
        }

        .name-item input,
        .name-item div {
            width: calc(50% - 20px);
        }

        .name-item div input {
            width: 97%;
        }

        .name-item div label {
            display: block;
            padding-bottom: 5px;
        }

        #ssd {
            resize: none;
        }
    }

    .file-upload {
        background-color: #ffffff;
        width: 600px;
        margin: 0 auto;
        padding: 20px;
    }

    .file-upload-btn {
        width: 100%;
        margin: 0;
        color: #fff;
        background: #1FB264;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #15824B;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .file-upload-btn:hover {
        background: #1AA059;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .file-upload-btn:active {
        border: 0;
        transition: all .2s ease;
    }

    .file-upload-content {
        display: none;
        text-align: center;
    }

    .file-upload-input {
        position: absolute;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        outline: none;
        opacity: 0;
        cursor: pointer;
    }

    .image-upload-wrap {
        margin-top: 20px;
        border: 4px dashed #1FB264;
        position: relative;
    }

    .image-dropping,
    .image-upload-wrap:hover {
        background-color: #1FB264;
        border: 4px dashed #ffffff;
    }

    .image-title-wrap {
        padding: 0 15px 15px 15px;
        color: #222;
    }

    .drag-text {
        text-align: center;
    }

    .drag-text h3 {
        font-weight: 100;
        text-transform: uppercase;
        color: #15824B;
        padding: 60px 0;
    }

    .file-upload-image {
        max-height: 200px;
        max-width: 200px;
        margin: auto;
        padding: 20px;
    }

    .remove-image {
        width: 200px;
        margin: 0;
        color: #fff;
        background: #cd4535;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #b02818;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .remove-image:hover {
        background: #c13b2a;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .remove-image:active {
        border: 0;
        transition: all .2s ease;
    }
</style>

@endsection


@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="row">

        <div class="testbox">
            <form action="{{ route('updateProject', $projectData->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset>

                    <div class="item">
                        <label for="">Name and Location of the Project</label>
                        <textarea name="namelocation" id="ssd" rows="2">{{$projectData->namelocation}}</textarea>
                    </div>
                    <div class="item">
                        <label for="">Project Descriptions</label>
                        <textarea name="project_description" id="summary-ckeditor" rows="3">{{$projectData->project_description}}</textarea>
                    </div>
                    <div class="item">
                        <label for="address">Project Cost in (Tk.)</label>
                        <input value="{{$projectData->project_cost}}" name="project_cost" id="address" type="text" name="address" />
                    </div>
                    <div class="columns">
                        <div class="form-group item">
                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">
                                Start Date</label>
                            <div class="col-sm-8">
                                <input value="{{$projectData->start_date}}" type="text" class="date-picker form-control" id="start_date" name="start_date" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="Start Date: yyyy-mm-dd">

                                {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        <div class="form-group item">
                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">
                                End Date</label>
                            <div class="col-sm-8">
                                <input value="{{$projectData->end_date}}" type="text" class="date-picker form-control" id="end_date" name="end_date" value="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" placeholder="Start Date: yyyy-mm-dd">

                                {!! $errors->first('start_date', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <label for="">Services Renders by the firm</label>
                        <textarea name="service_render" id="ssd" rows="2">{{$projectData->service_render}}</textarea>
                    </div>
                    <div class="item">
                        <label for="eaddress">Remarks</label>
                        <input value="{{$projectData->remarks}}" name="remarks" id="eaddress" type="text" name="eaddress" />
                    </div>

                    <div class="file-upload">
                        <div class="image-upload-wrap">
                            <input name="n_file" class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                            <div class="drag-text">
                                <h3>Drag and drop a file or select to add File</h3>
                            </div>
                        </div>
                        <div class="file-upload-content">
                            <img class="file-upload-image" src="#" alt="" />
                            <div class="image-title-wrap">
                                <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                            </div>
                        </div>
                    </div>

                    @php
                    $extention_name = pathinfo($projectData->n_file, PATHINFO_EXTENSION)
                    @endphp

                    @if($extention_name == 'png' || $extention_name == 'jpg' || $extention_name == 'jpeg')
                    <div>
                        <center>
                            <div class="form-group">
                                <label class=""></label>
                                <img src="{{ '/'.$projectData->n_file}}" height="250px" width="300px">
                            </div>
                        </center>
                    </div>
                    @elseif($extention_name == 'docx' || $extention_name == 'doc' || $extention_name == 'pdf')
                    <div>
                        <center>
                            <div class="form-group">
                                <label class=""></label>

                                <iframe src="{{'/'.$projectData->n_file}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>

                                <!-- to show pdf & doc/x in live server -->
                                <!-- <iframe src="https://docs.google.com/viewer?embedded=true&url=<?php echo '/' . ($projectData->n_file); ?>" frameborder="no" style="width:100%;height:400px"></iframe> -->
                            </div>
                        </center>
                    </div>

                    @else

                    @endif
                </fieldset>

                <div class="btn-block">
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
@endsection