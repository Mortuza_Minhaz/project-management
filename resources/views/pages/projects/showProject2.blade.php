@extends('layouts.app')

@section('title', 'Project List')


@section('js')

<script>

</script>


@endsection

@section('css')


@endsection



@section('content')
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->


    @include('pages.include.beginPageHeader')


    <!-- END PAGE HEADER-->

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                <a href="javascript:;" class="remove" data-original-title="" title=""></a>
            </div>
        </div>
        <div class="portlet-body flip-scroll">
            <table class="table table-bordered table-condensed flip-content">
                <tr>
                    <td colspan="2">Assignment</td>
                    <td colspan="1">Country</td>
                </tr>
                <tr>
                    <td colspan="2">Assignment</td>
                    <td colspan="1">Country</td>
                </tr>
                <tr>

                    <td colspan="2">Name of client</td>
                    <td rowspan="3">professional</td>

                </tr>
                <tr>
                    <td>Start Date</td>
                    <td>Completion</td>

                </tr>
                <tr>
                    <td>jan</td>
                    <td>Feb</td>
                </tr>
                <tr>
                    <td colspan="2">Name of joint</td>
                    <td colspan="1">No of staff</td>
                </tr>
                <tr>
                    <td colspan="3">Name of staff</td>

                </tr>
                <tr>
                    <td colspan="3">Detailed</td>

                </tr>
                <tr>
                    <td colspan="3">Detailed description</td>
                </tr>

            </table>

            <table class="table table-bordered">
                <tr>
                    <td colspan="1" style="width:20%">Assignment</td>
                    <td colspan="2">Country</td>
                </tr>
                <tr>
                    <td colspan="1" style="width:20%">Assignment</td>
                    <td colspan="2">Country</td>
                </tr>
            </table>
        </div>
    </div>



@endsection