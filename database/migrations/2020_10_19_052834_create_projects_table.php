<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->longText('project_name');
            $table->string('country')->nullable();
            $table->string('in_location')->nullable();
            $table->string('client_name')->nullable();
            $table->string('staff_months')->nullable();
            $table->string('jv_consultant')->nullable();  
            $table->string('jv_staff_months')->nullable();          
            $table->longText('project_description');
            $table->string('project_cost');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->longText('service_render');
            $table->string('remarks')->nullable();
            $table->string('firm_name');
            $table->string('assign_staffs')->nullable();
            $table->string('n_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
