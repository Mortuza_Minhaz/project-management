<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCvStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv_staff', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->string('consultant_name')->nullable();
            $table->string('rfp_no')->nullable();
            $table->longText('client_name')->nullable();
            $table->string('position');
            $table->string('staff_name');  
            $table->date('birth_date')->nullable();
            $table->string('nationality')->nullable();
            $table->string('membership')->nullable();
            $table->longText('qualification');
            $table->date('experience');
            $table->string('training')->nullable();
            $table->string('country_w_experience')->nullable();
            $table->string('employer')->nullable();
            $table->string('duration')->nullable();
            $table->string('employment_position')->nullable();
            $table->longText('duties')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cv_staff');
    }
}
