<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\CvStaffController;
use App\Http\Controllers\Staff_CategoryController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);

    Route::any('projectList', [ProjectController::class, 'index'])->name('project');

    Route::get('project/create',[ProjectController::class, 'create'])->name('addProject');

    Route::post('insert',[ProjectController::class, 'insert'])->name('insertProjectinfo');

    Route::get('show/{id}',[ProjectController::class, 'show'])->name('showProject');

    Route::get('/project/{project}/edit', [ProjectController::class, 'edit'])->name('editProject');

    Route::post('/project/{project}/update', [ProjectController::class, 'update'])->name('updateProject');

    Route::get('/project/{project}/delete', [ProjectController::class, 'delete'])->name('deleteProject');

    Route::any('cvList', [CvStaffController::class, 'index'])->name('staffcv');

    Route::get('staffCV/create', [CvStaffController::class, 'create'])->name('addStaffcv'); 

    Route::get('/staffCV/{staffCV}/edit', [CvStaffController::class, 'edit'])->name('editStaffcv');

    Route::post('/staffCV/{staffCV}/update', [CvStaffController::class, 'update'])->name('updateStaffcv');

    Route::get('/staffCV/{staffCV}/delete', [CvStaffController::class, 'destroy'])->name('deleteStaffcv');

    Route::post('staffCV/insert', [CvStaffController::class, 'store'])->name('storeStaffcv');

    Route::any('CV/category', [Staff_CategoryController::class, 'index'])->name('category');

    Route::post('CV/category/insert', [Staff_CategoryController::class, 'store'])->name('storeCategory');

    Route::post('CV/category/{category}/update', [Staff_CategoryController::class, 'update'])->name('updateStaffcvCategory');

    Route::get('CV/category/{category}/edit', [Staff_CategoryController::class, 'edit'])->name('editStaffcvCategory');

    Route::get('CV/category/{category}/delete', [Staff_CategoryController::class, 'destroy'])->name('deleteStaffcvCategory');



  
});

